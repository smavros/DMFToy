from dmft.params import *
from dmft.scc import selfConsistentChemicalPot
from dmft.plot import plotEvolution

mu = 1.23    # Chemical potential (Arbitrary initial value)
beta = 10    # Inverse temperature 1/k_B*T
U = 1        # Coulomb interaction
N = 1        # Total number of electrons
magn = 0.00  # Magnetization
phys_params = PhysParams(mu, beta, U, N, magn)

t_p = 50       # Number of time points
f_p = 1000     # Number of matsubara freqs
k_ppa = 200    # k-points per axis
tol = 1e-4     # Self consistent loop tolerance
comp_params = CompParams(t_p, f_p, k_ppa, scc_tol=tol)

selfConsistentChemicalPot(comp_params, phys_params, plot=True)

plotEvolution(comp_params, phys_params,
              fname="self_converged_mu_Greens_fun.png")
