from enum import Enum, auto
import inspect


class Spin(Enum):
    UP = auto()
    DOWN = auto()


class PhysParams:
    def __init__(self, mu, beta, U, Ne=1, magnetization=0, spin=Spin.UP,
                 lattice_constant=1, hopping_parameter=1):
        self.a = lattice_constant
        self.t = hopping_parameter
        self.mu = mu
        self.beta = beta
        self.U = U
        self.N = Ne
        if self.N > 2:
            raise Exception("You cannot have more that 2 fermions")
        self.magn = magnetization
        if abs(self.magn) > self.N / 2:
            raise Exception("Illegal value of magnetization")
        self.spin = spin
        if not isinstance(spin, Spin):
            raise Exception("The spin can be Spin.UP or Spin.DOWN")
        # Set n_sigma
        if self.spin is Spin.UP:
            self.n_sigma = self.N / 2 + self.magn
        else:
            self.n_sigma = self.N / 2 - self.magn
            

    def __str__(self):
        return ("$\\alpha=${}, $t=${}, $\\beta=${}, "
                "$U=${}, $m=${:.2f}, $N_e=${}, "
                "$\\mu=${:.3f}").format(self.a, self.t, self.beta, self.U,
                                        self.magn, self.N, self.mu)

    def nobeta_toStr(self):
        return ("$\\alpha=${}, $t=${}, "
                "$\\mu=${:.3f}").format(self.a, self.t, self.mu)


class CompParams:
    def __init__(self, time_points, freq_points, k_points_per_axis,
                 scc_tol=1e-6, scc_mix_factor=2, scc_max_iter=40):
        self.tp = time_points
        # Make sure that we have EVEN number of frequencies
        self.fp = freq_points + freq_points % 2
        # Make sure that we have ODD number of k points (to include origin)
        self.kp = k_points_per_axis + (k_points_per_axis - 1) % 2
        self.scc_tol = scc_tol
        self.scc_mix_factor = scc_mix_factor
        self.scc_max_iter = scc_max_iter

    def __str__(self):
        return ("$k_{{points/axis}}=${}, "
                "$f_{{points}}$={}").format(self.kp, self.fp)

    def kp_toStr(self):
        return "$k_{{points/axis}}=${}".format(self.kp)
