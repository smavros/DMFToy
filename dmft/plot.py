import matplotlib.pyplot as plt
import numpy as np
from .greens import spectrumLocalGreenFunc2D, evolutionLocalGreenFunc2D, \
    evolutionLocalGreenFunc2D_AsymptoticApprox


def plotSpectrum(comp_params, phys_params, k_values, fname="spectrum.png"):
    # If k_value is scalar turn it to list
    if not k_values:
        k_values = [comp_params.kp]
    # Save number of k points from params 
    kp_in_params = comp_params.kp
    # Calculate G(tau)
    fig, (ax0, ax1) = plt.subplots(nrows=2)
    for points in k_values:
        comp_params.kp = points
        freq, value = spectrumLocalGreenFunc2D(comp_params, phys_params)
        ax0.plot(freq, value.real, '.-', label=str(points) + '$^2$ $k$ points')
        ax1.plot(freq, value.imag, '.-', label=str(points) + '$^2$ $k$ points')
    # Plot data
    ax0.set(ylabel='$\mathcal{Re}\; G^0_\sigma(\\nu_n)$')
    ax0.legend(loc='lower left')
    ax0.grid()
    ax1.set(xlabel='$\\nu_n$', ylabel='$\mathcal{Im}\; G^0_\sigma(\\nu_n)$')
    ax1.legend(loc='lower left')
    ax1.grid()
    plt.suptitle(str(phys_params) + '\n' + str(comp_params), fontsize=10)
    plt.subplots_adjust(top=0.9, left=0.2)
    plt.savefig(fname, dpi=150, format="png")
    # Restore number of k points from params 
    comp_params.kp = kp_in_params


def plotEvolution(comp_params, phys_params, f_values=0,
                  asymptotic_approx=True, fname="evolution.png"):
    ''' By default plots the time evolution of the Local Green Function 
        calculated with asympotic approximation to speedup the convergency.
    '''
    # If f_values is scalar turn it to list
    if not f_values:
        f_values = [comp_params.fp]
    # Save number of freq from params 
    fp_in_params = comp_params.fp
    # Calculate G(inu)
    fig, ax = plt.subplots()
    for fs in f_values:
        comp_params.fp = fs
        if asymptotic_approx:
            freq, value = evolutionLocalGreenFunc2D_AsymptoticApprox(
                comp_params, phys_params)
        else:
            freq, value = evolutionLocalGreenFunc2D(comp_params, phys_params)
        ax.plot(freq, value.real, '.-', label=str(fs) + ' freqs')
    # Plot data
    ax.legend(loc='lower left')
    ax.grid()
    title = str(phys_params) + '\n' + comp_params.kp_toStr()
    if asymptotic_approx:
        title += ' (Asymp.Approx)'
    plt.title(title, fontsize=10)
    plt.xlabel('$\\tau_n$')
    plt.ylabel('$G^0_\sigma(\\tau_n)$')
    plt.savefig(fname, dpi=150, format="png")
    # Restore number of freq from params 
    comp_params.fp = fp_in_params


def plotEvolutionInBetaRange(beta, comp_params, phys_params,
                             fname="evolution_beta_range.png"):
    fig, ax = plt.subplots()
    # Save beta form params
    beta_in_params = phys_params.beta
    for b in beta:
        phys_params.beta = b
        freq, value = evolutionLocalGreenFunc2D_AsymptoticApprox(
            comp_params, phys_params)
        ax.plot(freq, value.real, '-', label='$\\beta=$' + str(b))
    ax.legend(loc='lower left')
    ax.grid()
    plt.title(phys_params.nobeta_toStr() + '\n' + str(comp_params)
              + ' (Asymp.Approx.)', fontsize=10)
    plt.xlabel('$\\tau_n$')
    plt.ylabel('$G^0_\sigma(\\tau_n)$')
    plt.savefig(fname, dpi=150, format="png")
    # Save beta form params
    phys_params.beta = beta_in_params


def plotSelfConsistentCalculationConvergence(buf, fname="scc_convergence.png"):
    fig, ax = plt.subplots()
    ax.plot(np.arange(0, buf.size), buf, '-')
    plt.title('Convergence of Self Consistent Calculation of $\mu$')
    plt.xlabel('step')
    plt.ylabel('residual')
    plt.savefig(fname, dpi=150, format="png")
