import numpy as np


def dispersionRelation(a, t, k_x, k_y):
    return -2.0 * t * (np.cos(k_x * a) + np.cos(k_y * a))


def nonInteractingMatsubaraGreenFunc2D(freq, k_x, k_y, phys_params):
    return 1 / (1j * freq
                - dispersionRelation(phys_params.a, phys_params.t, k_x, k_y)
                - phys_params.U * (phys_params.N - phys_params.n_sigma)
                + phys_params.N * phys_params.U * 0.5
                + phys_params.mu)


def localGreenFunc2D(freq, comp_params, phys_params):
    """ Calculates the local Green Function for a 2D system with square 1st
        Brillouin zone. Will always use an odd number of k-points per axis
    """
    zone_limit = np.pi / phys_params.a
    total_points = comp_params.kp * comp_params.kp
    X = np.linspace(-zone_limit, zone_limit, comp_params.kp, endpoint=True)
    Y = np.linspace(-zone_limit, zone_limit, comp_params.kp, endpoint=True)
    X, Y = np.meshgrid(X, Y)
    return np.sum(nonInteractingMatsubaraGreenFunc2D(freq, X, Y, phys_params),
                  dtype=np.complex) / total_points


def spectrumLocalGreenFunc2D(comp_params, phys_params):
    """ Returns the spectrum of the Local Green Function for the values of 
        frequency [-f_n, -f_n+2, ... ,f_n-2, f_n] where each value n is even
        (step=2). The antisymmetry property is used in order to do only half 
        of the calculations.
    """
    positive_fp = comp_params.fp // 2  # total number of positive freq
    n = np.linspace(0, positive_fp - 1, num=positive_fp, endpoint=True,
                    dtype=int)
    freq = (2 * n + 1) * np.pi / phys_params.beta
    value = np.empty(positive_fp, dtype=np.complex)
    for i in range(positive_fp):
        value[i] = localGreenFunc2D(freq[i], comp_params, phys_params)
    # Use antisymmetry property
    value = np.concatenate((np.conj(np.flip(value, 0)), value))
    freq = np.concatenate(((-1) * np.flip(freq, 0), freq))
    return freq, value


def evolutionLocalGreenFunc2D(comp_params, phys_params):
    """ Returns the time evolution of the Local Green Function for the values
        times [-n, ... , n]. The antisymmetry property is used in order to do
        only half of the calculations.
    """
    tau = np.linspace(0, phys_params.beta, num=comp_params.tp,
                      endpoint=True)
    freq, val = spectrumLocalGreenFunc2D(comp_params, phys_params)
    value = np.empty(np.shape(tau), dtype=np.complex)
    for i in range(comp_params.tp):
        exponents = np.exp(-1j * tau[i] * freq)
        value[i] = np.dot(val, exponents) / phys_params.beta
    # Use antisymmetry property
    value = np.concatenate(((-1) * np.flip(value, 0), value))
    tau = np.concatenate(((-1) * np.flip(tau, 0), tau))
    return tau, value


def evolutionLocalGreenFunc2D_AsymptoticApprox(comp_params, phys_params):
    """ Returns the time evolution of the Local Green Function for the values
        times [-n, ... , n]. The antisymmetry property is used in order to do
        only half of the calculations. Additionaly we correct the spectral sum
        with an asymptotic approximation for hight frequnecy values thus the 
        value of the Local Green Function converges must faster.
    """
    tau = np.linspace(0, phys_params.beta, num=comp_params.tp, endpoint=True)
    freq, val = spectrumLocalGreenFunc2D(comp_params, phys_params)
    value = np.empty(np.shape(tau), dtype=np.complex)
    for i in range(comp_params.tp):
        exponents = np.exp(-1j * tau[i] * freq)
        value[i] = -0.5 + np.dot(val + 1j / freq, exponents) / phys_params.beta
    # Use antisymmetric property
    value = np.concatenate(((-1) * np.flip(value, 0), value))
    tau = np.concatenate(((-1) * np.flip(tau, 0), tau))
    return tau, value
