import numpy as np
from .greens import evolutionLocalGreenFunc2D_AsymptoticApprox
from .plot import plotSelfConsistentCalculationConvergence


def selfConsistentChemicalPot(comp_params, phys_params, plot=False):
    print("-= Self Consistent Calculation =-")
    # Save time points value from params
    time_points_temp = comp_params.tp
    # Calculate local Green function only at 0^+
    comp_params.tp = 1
    scc_iter = 1
    residuals_arr = np.array([])
    while True:
        # Calculate n_sigma from G(tau) at tau->0^+
        val = evolutionLocalGreenFunc2D_AsymptoticApprox(comp_params,
                                                         phys_params)[1]
        n_sigma = val.real[1] + 1  # val = [G(t->0^-), G(t->0^+)]
        residual = phys_params.n_sigma - n_sigma
        residuals_arr = np.append(residuals_arr, residual)
        # Update chemical potential and magnetization
        phys_params.mu += comp_params.scc_mix_factor * residual
        phys_params.magn = n_sigma - phys_params.N * 0.5
        # Print info
        print(" loop: {} residual: {}".format(scc_iter, residual))
        # Check stoping criteria
        if abs(residual) < comp_params.scc_tol:
            print('Self Consistent Calculation CONVERGED')
            print("converged chemical potential {:.5f}".format(phys_params.mu))
            print("magnetization {:.5f}".format(phys_params.magn))
            break
        elif scc_iter >= comp_params.scc_max_iter:
            print('Self Consistent Calculation FAILED (max iterations)')
            break
        # Increment iteration
        scc_iter += 1
    # Restore time points value in params
    comp_params.tp = time_points_temp
    # Convergence Plot
    if plot:
        plotSelfConsistentCalculationConvergence(residuals_arr)
