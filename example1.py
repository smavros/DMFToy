from dmft.params import *
from dmft.plot import *

mu = 0.0    # Chemical potential
beta = 10   # Inverse temperature 1/k_B*T
U = 0       # Coulomb interaction
magn = 0.0  # Magnetization
N = 1       # Total number of electrons
a = 1       # Lattice constant
t = 1       # Hopping parameter
spin = Spin.UP
phys_params = PhysParams(mu, beta, U, N, magn, spin, a, t)

t_p = 80     # Number of time points
f_p = 300    # Number of matsubara freqs
k_ppa = 201  # k-points per axis
comp_params = CompParams(t_p, f_p, k_ppa)

# Spectrum of local Green function
plotSpectrum(comp_params, phys_params, k_values=[5, 50, 500])

# Evolution of local Green function
plotEvolution(comp_params, phys_params, asymptotic_approx=False,
              f_values=[5, 50, 500])

# Evolution of local Green function with asympotic approach
plotEvolution(comp_params, phys_params, f_values=[5, 50, 500],
              fname="evolution_asym.png")

# Dependence on beta
plotEvolutionInBetaRange([10, 30, 50, 100], comp_params, phys_params)
